#!./env/bin/python

import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/database-programs/databases/')
isRunning = True

print(files_path)
# Create and connect to database
conn = sqlite3.connect(files_path / 'chinook.db')
cur = conn.cursor()

while isRunning:
    print("[1] Print database")
    print("[2] Insert to table")
    print("[3] Update table")
    print("[4] Delete from table")
    
    option = input("select option: ")
    match option:
        case "1":
            # print out the values
            with conn:
                cur.execute("SELECT * FROM genres")
            for row in cur:
                print(row)
        case "2":
            with conn:
                cur.execute('INSERT INTO genres (GenreId, Name) VALUES (?, ?)', ('1', "Metal"))
        #case 3:

            #return
        #case 4:
        case default:
            quit()

# Close database connection
conn.close()

