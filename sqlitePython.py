#!./env/bin/python

import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/database-programs/databases/')
isRunning = True

print(files_path)
# Create and connect to database
conn = sqlite3.connect(files_path / 'chinook.db')
cur = conn.cursor()

def ChooseTable():
    print("select table")
    with conn:
        cur.execute("SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%'")
    for row in cur:
        print(row)
    return input("\nType in name of table:\n")

while isRunning:
    print("[1] Print table")
    print("[2] Insert record")
    print("[3] Update record (Not implemented)")
    print("[4] Delete record (Not implemented)")
    print("[5] Print customers not from USA")
    print("[6] Print customers from Brazil")
    print("[7] Print invoices from customers in Brazil")
    
    option = input("select option: ")
    match option:
        case "1":
            cmd = f"SELECT * FROM {ChooseTable()}"
            # print out the values
            with conn:
                cur.execute(cmd)
            for row in cur:
                print(row)
        case "2":
            print("Will only work with the following tables:\n")
            print("media_types\ngenres\nplaylists\nplaylist_track\nartists")
            Table = ChooseTable()
            cmd = f"SELECT sql FROM sqlite_schema WHERE name = '{Table}'"
            with conn:
                cur.execute(cmd)
            for row in cur:
                print(row)

            cmd = f"INSERT INTO {Table} Values (?, ?)"
            values = [0] * 2
            values[0] = input("Type Id: ")
            values[1] = input("Type value: ")
            with conn:
                cur.execute(cmd, (values[0], values[1]))
        case "3":
            quit()
        case "4":
            quit()
        case "5":
            cmd = f"SELECT customerId, FirstName, LastName, Country FROM customers WHERE NOT Country='USA'"
            with conn:
                cur.execute(cmd)
            for row in cur:
                print(row)
        case "6":
            cmd = f"SELECT customerId, FirstName, LastName, Country FROM customers WHERE Country='Brazil'"
            with conn:
                cur.execute(cmd)
            for row in cur:
                print(row)
        case "7":
            cmd = f"SELECT invoices.InvoiceId, invoices.InvoiceDate, invoices.BillingCountry, customers.FirstName, customers.LastName FROM invoices INNER JOIN customers ON invoices.CustomerId=customers.CustomerId"
            with conn:
                cur.execute(cmd)
            for row in cur:
                print(row)
        case default:
            isRunning = False

# Close database connection
conn.close()

